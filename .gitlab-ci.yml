workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: "$CI_MERGE_REQUEST_IID"
    # For default branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    # - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # Temporarly create Ci for every branch:
    - if: "$CI_COMMIT_BRANCH"
    # For tags, create a pipeline.
    - if: "$CI_COMMIT_TAG"
    # For stable branches, create a pipeline.
    - if: '$CI_COMMIT_BRANCH =~ /^[\d-]+-stable$/'
    #

default:
  image: golang:1.18

stages:
  - test
  - release

variables:
  SAST_EXCLUDED_ANALYZERS: "bandit,gosec,eslint"

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

code_navigation:
  stage: test
  image: golang:latest
  allow_failure: true
  script:
    - go install github.com/sourcegraph/lsif-go/cmd/lsif-go@latest
    - lsif-go
  artifacts:
    reports:
      lsif: dump.lsif

run_tests:
  stage: test
  before_script:
    ## Adding private SSH key to the executor, more information: https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-docker-executor
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 600 ~/.ssh/known_hosts
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

  script:
    # `goreleaser` also uses GITLAB_TOKEN and so we need to distinguish between
    # the GITLAB_TOKEN_TEST with less privilege used for testing and the GITLAB_TOKEN_RELEASE token
    - GITLAB_TOKEN=$GITLAB_TOKEN_TEST make test

secret_detection:
  rules:
    - if: $SECRET_DETECTION_DISABLED
      when: never
    - if: '$CI_MERGE_REQUEST_IID'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: []
  dependencies: [] # Don't download artifacts, especially `./public/`

.release:
  stage: release
  image: docker:stable
  services:
    - docker:dind
  variables:
    # Disable shallow cloning so that goreleaser can diff between tags to
    # generate a changelog.
    GIT_DEPTH: 0

release_test:
  extends: .release
  except:
    - tags
  script: |
    docker run --rm --privileged \
      -v $PWD:/go/src/gitlab.com/gitlab-org/cli \
      -w /go/src/gitlab.com/gitlab-org/cli \
      -v /var/run/docker.sock:/var/run/docker.sock \
      goreleaser/goreleaser release --snapshot --rm-dist

release:
  extends: .release
  only:
    - tags
  variables:
    DOCKER_REGISTRY: $CI_REGISTRY
    DOCKER_USERNAME: $CI_REGISTRY_USER
    DOCKER_PASSWORD: $CI_REGISTRY_PASSWORD

  script: |
    docker run --rm --privileged \
      -v $PWD:/go/src/gitlab.com/gitlab-org/cli \
      -w /go/src/gitlab.com/gitlab-org/cli \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -e DOCKER_USERNAME -e DOCKER_PASSWORD -e DOCKER_REGISTRY  \
      -e GITLAB_TOKEN=$GITLAB_TOKEN_RELEASE \
      goreleaser/goreleaser release
